module.exports = {
  plugins: ['react-native'],
  env: {
    'react-native/react-native': true,
  },
  rules: {
    'react-native/no-unused-styles': 'warn',
    'no-use-before-define': ['error', { variables: false }], // added for styles
  },
  overrides: [
    {
      files: ['**/*.ts?(x)'],
      rules: {
        /**
         * Base rule must be disabled to avoid incorrect errors.
         * https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/no-use-before-define.md
         */
        'no-use-before-define': 'off',
        '@typescript-eslint/no-use-before-define': [
          'error',
          { variables: false },
        ], // added for styles
      },
    },
  ],
};
