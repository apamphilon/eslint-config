module.exports = {
  // View link below for react rules documentation
  // https://github.com/jsx-eslint/eslint-plugin-react#list-of-supported-rules
  rules: {
    'react/prop-types': 'warn',
  },
  extends: ['plugin:react/jsx-runtime'], // Added to disable react-in-jsx-scope - https://github.com/jsx-eslint/eslint-plugin-react/blob/master/docs/rules/react-in-jsx-scope.md#when-not-to-use-it
};
