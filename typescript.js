module.exports = {
  extends: [
    'airbnb',
    './rules/prettier',
    './rules/react',
    './rules/react-hooks',
  ],
  overrides: [
    {
      files: ['**/*.{ts,tsx}'],
      parser: '@typescript-eslint/parser',
      extends: [
        'airbnb-typescript',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
      ],
      /**
       * Inspired by:
       * https://github.com/kentcdodds/eslint-config-kentcdodds/blob/main/index.js#L240
       * https://github.com/facebook/create-react-app/blob/main/.eslintrc.json#L10
       */
      parserOptions: {
        ecmaVersion: 2018,
        project: './tsconfig.json',
        sourceType: 'module',
      },
      rules: {
        'react/jsx-filename-extension': [
          'warn',
          { extensions: ['.js', '.jsx', '.ts', '.tsx'] },
        ],
        'react/prop-types': 'off',
        'react/require-default-props': 'off',
        '@typescript-eslint/consistent-type-exports': 'error',
        '@typescript-eslint/consistent-type-imports': 'error',
        '@typescript-eslint/method-signature-style': ['error', 'property'],
        '@typescript-eslint/no-empty-interface': [
          'error',
          {
            allowSingleExtends: false,
          },
        ],
        '@typescript-eslint/array-type': 'error',
        '@typescript-eslint/consistent-indexed-object-style': [
          'error',
          'record',
        ],
        '@typescript-eslint/await-thenable': 'error',
        '@typescript-eslint/ban-types': 'error',
      },
    },
  ],
  /**
   * Inspired by:
   * https://github.com/kentcdodds/eslint-config-kentcdodds/blob/main/index.js#L11
   * https://github.com/facebook/create-react-app/blob/main/.eslintrc.json#L3
   */
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
};
