module.exports = {
  /**
   * Jest and testing library don't need to be in the plugins, as they are
   * included in the extends below e.g plugin:jest/recommended and
   * plugin:testing-library/react.
   * Add plugins ['jest'] if removing plugin:jest/recommended.
   */
  // plugins: ['jest'],
  /**
   * Added env jest/globals as we have jest globals in folders that don't
   * match the glob pattern for 'plugin:jest/recommended',
   * i.e our __mocks__ folder in the Airtime Rewards mobile app.
   */
  env: {
    'jest/globals': true,
  },
  overrides: [
    {
      files: ['**/__tests__/**/*', '**/*.{spec,test}.*'],
      extends: ['plugin:jest/recommended', 'plugin:testing-library/react'],
      /**
       * jest-formatting plugin and jest-formatting rules have been added here
       * as there is currently a bug in plugin:jest-formatting/recommended
       * that results in these rules not being correctly applied.
       * The glob on line 150 of index.ts should include globstar/star
       * https://github.com/dangreenisrael/eslint-plugin-jest-formatting/blob/master/src/index.ts
       * Will report the issue on their repo soon.
       */
      plugins: ['jest-formatting'],
      rules: {
        'jest-formatting/padding-around-after-all-blocks': 2,
        'jest-formatting/padding-around-after-each-blocks': 2,
        'jest-formatting/padding-around-before-all-blocks': 2,
        'jest-formatting/padding-around-before-each-blocks': 2,
        'jest-formatting/padding-around-describe-blocks': 2,
        'jest-formatting/padding-around-test-blocks': 2,
        // https://github.com/jest-community/eslint-plugin-jest
        // https://github.com/testing-library/eslint-plugin-testing-library
      },
    },
  ],
};
