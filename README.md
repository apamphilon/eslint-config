# eslint-config

## Installation

`npm i @apamphilon/eslint-config`

## Project Setup

Add the following config to `package.json`:

```json
{
  "eslintConfig": {
    "extends": "@apamphilon/eslint-config"
  }
}
```

or `.eslintrc`/`.eslintrc.js`:

```json
{
  "extends": "@apamphilon/eslint-config"
}
```

## LICENCE

[MIT](LICENCE)
