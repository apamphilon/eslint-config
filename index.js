module.exports = {
  parser: '@babel/eslint-parser',
  parserOptions: {
    requireConfigFile: false,
  },
  extends: [
    'airbnb',
    './rules/prettier',
    './rules/react',
    './rules/react-hooks',
  ],
  /**
   * Inspired by:
   * https://github.com/kentcdodds/eslint-config-kentcdodds/blob/main/index.js#L11
   * https://github.com/facebook/create-react-app/blob/main/.eslintrc.json#L3
   */
  env: {
    browser: true,
    node: true,
    es6: true,
  },
};
